import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TarefasModule } from './tarefas/tarefas.module';

import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://db_user:viulwA0uzU1d1XTz@cluster0-it0tm.gcp.mongodb.net/test?retryWrites=true&w=majority'),
    TarefasModule,
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
