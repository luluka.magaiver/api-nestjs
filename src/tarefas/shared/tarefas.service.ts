import { Injectable } from '@nestjs/common';
import { Tarefa } from './tarefa';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class TarefasService {
  constructor(@InjectModel('Tarefa') private readonly tarefaModel: Model<Tarefa>) {}

  async getAll() {
    return await this.tarefaModel.find().exec();
  }

  async getById(id: string) {
    return await this.tarefaModel.findById(id).exec();
  }

  async create(t: Tarefa) {
    const tarefaCriada = new this.tarefaModel(t);
    return await tarefaCriada.save();
  }

  async update(id: string, t: Tarefa) {
    await this.tarefaModel.updateOne({ _id: id }, t).exec();
    return this.getById(id);
  }

  async delete(id: string) {
    return await this.tarefaModel.deleteOne({ _id: id }).exec();
  }
}
