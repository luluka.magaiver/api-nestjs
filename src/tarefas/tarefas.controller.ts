import { Controller, Get, Param, Body, Post, Put, Delete } from '@nestjs/common';
import { TarefasService } from './shared/tarefas.service';
import { Tarefa } from './shared/tarefa';

@Controller('tarefas')
export class TarefasController {
  constructor(private tarefaSrv: TarefasService) {}

  @Get()
  async getAll(): Promise<Tarefa[]> {
    return this.tarefaSrv.getAll();
  }

  @Get(':id')
  async getById(@Param('id') id: string): Promise<Tarefa> {
    return this.tarefaSrv.getById(id);
  }

  @Post()
  async create(@Body() tarefa: Tarefa): Promise<Tarefa> {
    return this.tarefaSrv.create(tarefa);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() tarefa: Tarefa): Promise<Tarefa> {
    return this.tarefaSrv.update(id, tarefa);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    this.tarefaSrv.delete(id);
  }
}
